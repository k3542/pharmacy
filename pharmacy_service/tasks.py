from celery import shared_task

from pharmacy_service.models import Recipe

import requests
import time
import json


@shared_task
def handle_recipes(request_id, recipes, context):
	time.sleep(30)

	medicines = Recipe.objects.filter(
			recipe__in=recipes
		).select_related(
			"medicine"
		).values_list(
			"medicine__name", 
			flat=True
		)

	api_url = "http://ec2-108-136-191-217.ap-southeast-3.compute.amazonaws.com:8000/apoteport/"
	headers =  {"Content-Type":"application/json"}

	data = {
		"id": request_id,
		"medicines": list(medicines),
		"context": context,
	}
	response = requests.post(api_url, data=json.dumps(data), headers=headers)