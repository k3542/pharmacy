from .views import PharmacyViews
from django.urls import path

app_name = 'pharmacy_service'

urlpatterns = [
    path("visit", PharmacyViews.as_view()),
]