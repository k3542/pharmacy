from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, request

from pharmacy_service.tasks import handle_recipes


class PharmacyViews(APIView):

	def post(self, request: request.Request, mitra_id=None):

		data = {**request.data}

		id = data["id"]
		recipes = data["recipe"]
		context = data["context"]

		handle_recipes.delay(id, recipes, context)

		return Response({
			"status": "OK",
		}, status.HTTP_200_OK)