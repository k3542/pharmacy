from django.core.management.base import BaseCommand, CommandError

from pharmacy_service.models import Medicine, Recipe

import random


class Command(BaseCommand):
	help = 'Prepopulate Fresh DB with neccessary data'

	def add_arguments(self, parser):
		pass

	def handle(self, *args, **options):
		medicines = Medicine.objects.all()
		recipes = Recipe.objects.all()

		if not medicines:
			Medicine.objects.create(name="Ibuprofen", price=5000, 
				benefit="Ibuprofen adalah obat antiinflamasi nonsteroid (NSAID). Mereka bekerja dengan menurunkan hormon penyebab peradangan dan rasa sakit di tubuh. Pereda nyeri ini digunakan untuk mengurangi demam, mengobati rasa sakit, atau peradangan yang disebabkan oleh berbagai kondisi.", 
				side_effect="Tiap obat berpotensi menyebabkan efek samping, termasuk ibuprofen. Beberapa efek samping yang dapat terjadi setalah menggunakan obat ini adalah:\n\n- Perut kembung\n- Mual dan muntah\n- Diare atau malah sembelit\n- Sakit maag\n- Pusing\n- Sakit kepala")
			Medicine.objects.create(name="Paracetamol", price=2500, 
				benefit="Paracetamol atau acetaminophen adalah obat analgesik non-opiat yang berfungsi untuk meredakan nyeri dan menurunkan demam. Nyeri yang dapat diatasi dengan paracetamol adalah nyeri ringan sampai sedang.\n\n Paracetamol dapat digunakan pada beberapa keluhan seperti nyeri kepala, nyeri otot, arthritis, nyeri punggung, nyeri pada gigi, dan demam. Paracetamol merupakan obat yang dijual bebas dan analgesik yang paling sering digunakan oleh dokter dan masyarakat sebagai lini pertama untuk meredakan nyeri.", 
				side_effect="Jika dikonsumsi sesuai anjuran dokter dan petunjuk penggunaan, paracetamol umumnya jarang menimbulkan efek samping. Namun, jika digunakan secara berlebihan dapat muncul beberapa efek samping berikut:\n\n- Sakit kepala\n- Mual atau muntah\n- Sulit tidur\n- Perut bagian atas terasa sakit\n- Urin berwarna gelap\n- Lelah yang tidak biasa\n")
			Medicine.objects.create(name="Diclofenac", price=10000, 
				benefit="Diclofenac merupakan obat antiinflamasi nonsteroid yang bekerja dengan cara menghambat produksi prostaglandin, yaitu zat yang memicu reaksi peradangan saat tubuh mengalami cedera atau luka.\n\n Dengan begitu gejala radang, seperti nyeri atau bengkak bisa mereda.", 
				side_effect="Ada beberapa efek samping yang bisa terjadi setelah menggunakan diclofenac, di antaranya:\n\n- Sakit perut atau heartburn\n- Mual atau kembung\n- Diare atau sembelit\n- Pusing, kantuk, atau sakit kepala")
			Medicine.objects.create(name="Methyl", price=20000, 
				benefit="Methylprednisolone adalah obat untuk meredakan peradangan pada berbagai kondisi, termasuk radang sendi, radang usus, asma, psoriasis, lupus, hingga multiple sclerosis.\n\n Obat ini juga bisa digunakan dalam pengobatan reaksi alergi yang parah. Metilprednisolon atau methylprednisolone bekerja dengan cara mencegah tubuh melepaskan senyawa kimia yang memicu peradangan. Dengan begitu, gejala peradangan, seperti nyeri dan pembengkakan, akan berangsur mereda.", 
				side_effect="Efek samping yang muncul akibat penggunaan obat methylprednisolone tergantung pada jenis obatnya. Sejumlah efek samping yang umum terjadi adalah:\n\n- Mual atau muntah\n- Pusing\n- Sakit kepala\n- Perut kembung\n- Sakit maag atau heartburn\n- Nyeri otot\n- Nafsu makan menurun\n- Sulit tidur")
			Medicine.objects.create(name="Antacid", price=12000, 
				benefit="Antasida (antacid) adalah obat untuk meredakan gejala akibat sakit maag atau penyakit asam lambung. Antasida tersedia dalam bentuk tablet kunyah dan cairan suspensi yang umumnya bisa dibeli bebas tanpa resep dokter.\n\n Antasida bekerja dengan cara menetralisir asam lambung. Obat ini hanya bekerja saat kadar asam lambung meningkat. Dengan begitu, keluhan akibat naiknya asam lambung, seperti nyeri ulu hati, rasa panas di dada, mual, muntah, atau perut kembung akan mereda.", 
				side_effect="Secara umum, penggunaan antasida dapat menyebabkan efek samping yang ringan, seperti:\n\n- Perut kembung\n- Diare\n- Mual dan muntah\n- Kram perut\n- Sembelit")
			Medicine.objects.create(name="Anastrazole", price=5000, 
				benefit="Anastrozole adalah obat untuk mengatasi kanker payudara pada wanita yang sudah menopause. Anastrozole bisa diberikan kepada pasien yang kondisinya tidak kunjung membaik setelah menjalani pengobatan dengan tamoxifen.\n\n Anastrozole bekerja dengan cara menurunkan kadar estrogen dalam tubuh dengan menghambat kerja enzim aromatase. Dengan begitu, diharapkan ukuran tumor akan mengecil dan pertumbuhan sel kanker bisa dihambat.", 
				side_effect="Ada beberapa efek samping yang bisa muncul setelah mengonsumsi anastrozole, antara lain:\n\n- Hot flashes\n- Insomnia\n- Sakit kepala\n- Pusing\n- Mual atau muntah\n- Sakit perut\n- Konstipasi atau diare\n- Tidak nafsu makan\n- Berat badan bertambah\n- Lemas atau lelah")
			Medicine.objects.create(name="Amoxapine", price=100000, 
				benefit="Amoxapine merupakan obat yang digunakan untuk mengobati gejala depresi, kecemasan, atau agitasi (resah dan gelisah). Mengobati depresi bisa meningkatkan mood, kebahagiaan, dan kualitas hidup penderitanya.", 
				side_effect="Selain memiliki efek yang diinginkan, setiap obat pasti memiliki efek samping yang tidak diinginkan. Untuk amoxapine, efek sampingnya dibedakan menjadi dua golongan, yaitu efek samping yang agak jarang terjadi dan yang jarang terjadi.\n\n Untuk efek samping yang jarang terjadi diantaranya, kegembiraan berlebih, detak jantung cepat dan tidak beraturan, kecemasan atau ketakutan, perubahan mood dan mental, mimpi buruk, sulit untuk tenang, sempoyongan saat berjalan, kurang tidur, bengkak, gangguan tidur, dan tidak bisa tidur. Sementara itu, efek samping amoxapine yang jarang terjadi antara lain perut kembung, kesemutan, konstipasi, pening, mengantuk, sakit kepala, kehilangan pendengaran, tekanan darah rendah, biduran, gangguan pencernaan, mual dan muntah, kulit kemerahan, tenggorokan nyeri, sakit perut, dan muntah.")
			Medicine.objects.create(name="Valsartan", price=250000, 
				benefit="Valsartan adalah obat untuk mengatasi hipertensi dan gagal jantung. Selain itu, obat ini juga terkadang digunakan dalam pengobatan setelah serangan jantung.\n\n Valsartan bisa ditemukan dalam bentuk tablet dan kaplet salut selaput sediaan 80 mg dan 160 mg. Valsartan bekerja dengan cara menghambat reseptor angiotensin II. Dengan begitu, pembuluh darah dapat melebar dan darah bisa mengalir dengan lebih lancar. Cara kerja ini akan membuat tekanan darah turun dan kerja  jantung dalam memompa darah dapat lebih baik.", 
				side_effect="Sejumlah efek samping yang bisa timbul setelah mengonsumsi valsartan adalah:\n\n- Pusing atau pusing berputar\n- Sakit kepala\n- Mual\n- Muntah\n- Diare\n- Otot atau sendi terasa sakit\n- Tekanan darah rendah")
			Medicine.objects.create(name="Letrozole", price=120000, 
				benefit="Letrozole adalah obat untuk mengobati kanker payudara pada wanita yang sudah menopause. Selain itu, obat ini juga bisa digunakan pada pasien kanker payudara yang sudah menjalani terapi radiasi atau bedah pengangkatan jaringan kanker.\n\n Latrozole termasuk ke dalam obat golongan nonsteroidal aromatase inhibitors yang bekerja dengan cara mengurangi jumlah estrogen yang diproduksi oleh tubuh, sehingga dapat memperlambat atau menghentikan pertumbuhan sel-sel kanker payudara.", 
				side_effect="Beberapa efek samping yang dapat timbul setelah mengonsumsi letrozole adalah:\n\n- Sensasi rasa panas (hot flashes)\n- Mual\n- Sakit kepala, pusing, atau kantuk\n- Keringat berlebihan\n- Nyeri sendi, tulang, atau otot\n- Rambut rontok\n- Berat badan bertambah\n- Sulit tidur \n- Peningkatan kadar kolesterol")
			Medicine.objects.create(name="Pilocarpine", price=110000, 
				benefit="Pilocarpine tetes mata adalah obat untuk menurunkan tekanan dalam bola mata pada glaukoma. Penurunan tekanan dalam bola mata (intraokular) ini dapat mengurangi risiko timbulnya kebutaan dan kerusakan saraf akibat glaukoma.\n\n Pilocarpine tetes mata merupakan obat agonis kolinergik yang bekerja langsung untuk mempengaruhi otot pada mata sehingga meningkatkan aliran dari cairan dalam bola mata. Cara kerja ini bisa menurunkan tekanan dalam bola mata. Selain itu, obat ini juga bisa mengecilkan ukuran pupil.", 
				side_effect="Efek samping yang mungkin timbul setelah menggunakan obat pilocarpine tetes mata antara lain:\n\n- Penglihatan buram\n- Sakit kepala atau nyeri di sekitar alis mata\n- Kesulitan melihat di tempat yang cahayanya redup\n- Rasa terbakar, gatal, atau perih selama sesaat, saat obat diteteskan di mata\n- Iritasi mata")

		medicine_random_order = Medicine.objects.order_by("?")
		medicine_len = len(medicine_random_order)

		if not recipes:
			Recipe.objects.create(recipe="rash", medicine=medicine_random_order[0 % medicine_len])
			Recipe.objects.create(recipe="pox", medicine=medicine_random_order[1 % medicine_len])
			Recipe.objects.create(recipe="coughing", medicine=medicine_random_order[2 % medicine_len])
			Recipe.objects.create(recipe="sneezing", medicine=medicine_random_order[3 % medicine_len])
			Recipe.objects.create(recipe="fever", medicine=medicine_random_order[4 % medicine_len])
			Recipe.objects.create(recipe="stomachache", medicine=medicine_random_order[5 % medicine_len])

		self.stdout.write(self.style.SUCCESS('Successfully prepopulating database'))