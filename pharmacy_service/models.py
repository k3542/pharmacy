from django.db import models


class Medicine(models.Model):
	name = models.CharField(max_length=255)
	price = models.IntegerField()
	benefit = models.TextField()
	side_effect = models.TextField()


class Recipe(models.Model):
	recipe = models.CharField(max_length=255, unique=True)
	medicine = models.ForeignKey(Medicine, on_delete=models.CASCADE)