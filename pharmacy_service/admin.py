from django.contrib import admin

from .models import Medicine, Recipe


admin.site.register(Medicine)
admin.site.register(Recipe)