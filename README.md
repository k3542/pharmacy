# Pharmacy Service

[![pipeline status][pipeline-badge]][commits-gl]

## Daftar Isi

- [Daftar Isi](#daftar-isi)
- [Informasi Kelompok](#informasi-kelompok)
- [Deskripsi](#deskripsi)

## Informasi Kelompok

Penanggung Jawab Pharmacy Service: Rico Tadjudin

Kelompok 8 LAW-B:
- Alisha Yumna Bakri - 1906400173
- Muhammad Fathan Muthahhari - 1906293190
- Muzaki Azami - 1806205470
- Rico Tadjudin - 1906398364
- Samuel Mulatua Jeremy - 1906308305

## Deskripsi

Pharmacy Service adalah servis yang akan memproses permintaan obat menggunakan sebuah resep baik bagi resep pribadi maupun resep yang didapatkan dari dokter/klinik.


[commits-gh]: https://github.com/k3542/pharmacy/commits/master
[pipeline-badge]: https://gitlab.com/k3542/pharmacy/badges/master/pipeline.svg
[commits-gl]: https://gitlab.com/k3542/pharmacy/-/commits/master